package grifts

import (
	"github.com/gobuffalo/buffalo"
	"gitlab.com/kkstream_portal/actions"
)

func init() {
	buffalo.Grifts(actions.App())
}
